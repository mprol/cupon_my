<?php

namespace Cupon\TiendaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Cupon\TiendaBundle\Form\Extranet\TiendaType;
use Cupon\OfertaBundle\Entity\Oferta;
use Cupon\OfertaBundle\Form\Extranet\OfertaType;

class ExtranetController extends Controller
{
    public function loginAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();

        $error = $peticion->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR,
            $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );

        return $this->render('TiendaBundle:Extranet:login.html.twig', array(
            'error' => $error
        ));
    }

    public function ofertaEditarAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $oferta = $em->getRepository('OfertaBundle:Oferta')->find($id);

        if (!$oferta) {
            throw $this->createNotFoundException('La oferta no existe');
        }

        $contexto = $this->get('security.context');
        if (false === $contexto->isGranted('EDIT', $oferta)) {
            throw new AccessDeniedException();
        }

        if ($oferta->getRevisada()) {
            $this->get('session')->setFlash('error',
                'La oferta no se puede modificar porque ya ha sido revisada'
            );
            return $this->redirect($this->generateUrl('extranet_portada'));
        }

        $peticion = $this->getRequest();
        $formulario = $this->createForm(new OfertaType(), $oferta);

        if ($peticion->getMethod() == 'POST') {
            $fotoOriginal = $formulario->getData()->getFoto();
            $formulario->bindRequest($peticion);

            if ($formulario->isValid()) {
                // La foto original no se modifica, recuperar su ruta
                if (null == $oferta->getFoto()) {
                    $oferta->setFoto($fotoOriginal);
                }
                // La foto de la oferta se ha modificado
                else {
                    $directorioFotos = $this->container->getParameter(
                        'cupon.directorio.imagenes'
                    );
                    $oferta->subirFoto($directorioFotos);

                    // Borrar la foto anterior
                    unlink($directorioFotos.$fotoOriginal);
                }

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($oferta);
                $em->flush();
                return $this->redirect(
                    $this->generateUrl('extranet_portada')
                );
            }
        }

        return $this->render('TiendaBundle:Extranet:formulario.html.twig',
            array(
                'accion' => 'editar',
                'oferta' => $oferta,
                'formulario' => $formulario->createView()
            )
        );
    }

    public function portadaAction()
    {
        $em = $this->getDoctrine()->getEntityManager();

        $tienda = $this->get('security.context')->getToken()->getUser();
        $ofertas = $em->getRepository('TiendaBundle:Tienda')
            ->findOfertasRecientes($tienda->getId());

        return $this->render('TiendaBundle:Extranet:portada.html.twig', array(
            'ofertas' => $ofertas
        ));
    }

    public function ofertaVentasAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $ventas = $em->getRepository('OfertaBundle:Oferta')
            ->findVentasByOferta($id);

        return $this->render('TiendaBundle:Extranet:ventas.html.twig', array(
            'oferta' => $ventas[0]->getOferta(),
            'ventas' => $ventas
        ));
    }

    public function ofertaNuevaAction()
    {
        $peticion = $this->getRequest();

        $oferta = new Oferta();
        $formulario = $this->createForm(new OfertaType(), $oferta);

        if ($peticion->getMethod() == 'POST') {
            $formulario->bindRequest($peticion);

            if ($formulario->isValid()) {
                // Completar las propiedades de la oferta que una tienda no puede establecer
                $tienda = $this->get('security.context')->getToken()->getUser();
                $oferta->setCompras(0);
                $oferta->setRevisada(false);
                $oferta->setTienda($tienda);
                $oferta->setCiudad($tienda->getCiudad());

                // Copiar la foto subida y guardar la ruta
                $oferta->subirFoto(
                    $this->container->getParameter('cupon.directorio.imagenes')
                );

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($oferta);
                $em->flush();

                // Asignar el permiso necesario para que la tienda pueda modificar esta oferta
//                $idObjeto  = ObjectIdentity::fromDomainObject($oferta);
//                $idUsuario = UserSecurityIdentity::fromAccount($tienda);

//                $acl = $this->get('security.acl.provider')->createAcl($idObjeto);
//                $acl->insertObjectAce($idUsuario, MaskBuilder::MASK_OPERATOR);
//                $this->get('security.acl.provider')->updateAcl($acl);

                return $this->redirect($this->generateUrl('extranet_portada'));
            }
        }

        return $this->render('TiendaBundle:Extranet:formulario.html.twig',
            array(
                'accion' => 'crear',
                'formulario' => $formulario->createView()
            )
        );
    }

    public function perfilAction()
    {
        $peticion = $this->getRequest();

        $tienda = $this->get('security.context')->getToken()->getUser();
        $formulario = $this->createForm(new TiendaType(), $tienda);

        if ($peticion->getMethod() == 'POST') {
            $passwordOriginal = $formulario->getData()->getPassword();
            $formulario->bindRequest($peticion);

            if ($formulario->isValid()) {
                // La tienda no cambia su contraseña, utilizar la original
                if (null == $tienda->getPassword()) {
                    $tienda->setPassword($passwordOriginal);
                }
                // La tienda cambia su contraseña, codificar su valor
                else {
                    $encoder = $this->get('security.encoder_factory')
                                    ->getEncoder($tienda);
                    $passwordCodificado = $encoder->encodePassword(
                        $tienda->getPassword(),
                        $tienda->getSalt()
                    );
                    $tienda->setPassword($passwordCodificado);
                }

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($tienda);
                $em->flush();
            }
        }

        return $this->render('TiendaBundle:Extranet:perfil.html.twig', array(
            'tienda' => $tienda,
            'formulario' => $formulario->createView()
        ));
    }
}