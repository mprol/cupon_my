<?php

namespace Cupon\BackendBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Output\Output;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\HttpFoundation\Request;

/**
 * Comando que envía cada día un email a todos los usuarios que lo
 * permiten con la información de la oferta del día en su ciudad
 *
 */
class EmailOfertaDelDiaCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('email:oferta-del-dia')
            ->setDefinition(array(
                new InputArgument('ciudad', InputArgument::OPTIONAL, 'El slug de la ciudad para la que se generan los emails'),
                new InputOption('accion', null, InputOption::VALUE_OPTIONAL, 'Indica si los emails sólo se generan o también se envían', 'enviar'),
            ))
            ->setDescription('Genera y envía a cada usuario el email con la oferta diaria')
            ->setHelp(<<<EOT
El comando <info>email:oferta-del-dia</info> genera y envía un email con la
oferta del día de la ciudad en la que se ha apuntado el usuario. También tiene
en cuenta si el usuario permite el envío o no de publicidad.
EOT
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $host = 'dev' == $input->getOption('env') ? 'http://cupon.local' : 'http://cupon.com';
        $accion = $input->getOption('accion');

        $contenedor = $this->getContainer();
        $em = $contenedor->get('doctrine')->getEntityManager();

        // Obtener el listado de usuarios que permiten el envío de email
        $usuarios = $em->getRepository('UsuarioBundle:Usuario')
                       ->findBy(array('permite_email' => true));

        $output->writeln(sprintf(
            'Se van a enviar <info>%s</info> emails', count($usuarios)
        ));

        // Buscar la 'oferta del día' en todas las ciudades de la aplicación
        $ofertas = array();
        $ciudades = $em->getRepository('CiudadBundle:Ciudad')->findAll();
        foreach ($ciudades as $ciudad) {
            $id = $ciudad->getId();
            $slug = $ciudad->getSlug();
            $ofertas[$id] = $em->getRepository('OfertaBundle:Oferta')
                               ->findOfertaDelDiaSiguiente($slug);
        }

        // Generar el email personalizado de cada usuario
        foreach ($usuarios as $usuario) {
            $ciudad = $usuario->getCiudad();
            $oferta = $ofertas[$ciudad->getId()];

            $contenido = $contenedor->get('twig')->render(
                'BackendBundle:Oferta:email.html.twig',
                array('host' => $host,
                    'ciudad' => $ciudad,
                    'oferta' => $oferta,
                    'usuario' => $usuario)
            );

            // Enviar el email
            if ('enviar' == $accion) {
                $mensaje = \Swift_Message::newInstance()
                    ->setSubject($oferta->getNombre().' en '.$oferta->getTienda()->getNombre())
                    ->setFrom(array('oferta-del-dia@cupon.com' => 'Cupon - Oferta del día'))
                    ->setTo($usuario->getEmail())
                    ->setBody($contenido, 'text/html')
                ;
                $contenedor->get('mailer')->send($mensaje);
            }
        }
    }
}