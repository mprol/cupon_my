<?php

namespace Cupon\UsuarioBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Security\Core\SecurityContext;
use Cupon\UsuarioBundle\Entity\Usuario;
use Cupon\UsuarioBundle\Form\Frontend\UsuarioType;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class DefaultController extends Controller
{
    public function defaultAction()
    {
        $usuario = new Usuario();
        $encoder = $this->get('security.encoder_factory')
                ->getEncoder($usuario);

        $password = $encoder->encodePassword(
                    $usuario->getPassword(),
                    $usuario->getSalt()
        );

        $usuario->setPassword($password);
    }

    public function loginAction()
    {
        $peticion = $this->getRequest();
        $sesion = $peticion->getSession();

        $error = $peticion->attributes->get(
            SecurityContext::AUTHENTICATION_ERROR,
            $sesion->get(SecurityContext::AUTHENTICATION_ERROR)
        );

        return $this->render('UsuarioBundle:Default:login.html.twig', array(
            'last_username' => $sesion->get(SecurityContext::LAST_USERNAME),
            'error' => $error
        ));
    }
    
    public function comprasAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        $usuario = $this->get('security.context')->getToken()->getUser();
        
        $cercanas = $em->getRepository('CiudadBundle:Ciudad')->findCercanas(
            $usuario->getCiudad()->getId()
        );
        
        $compras = $em->getRepository('UsuarioBundle:Usuario')->findTodasLasCompras($usuario->getId());
        
        return $this->render('UsuarioBundle:Default:compras.html.twig', array(
            'compras'  => $compras,
            'cercanas' => $cercanas
        ));
    }

    /**
     * Registra una nueva compra de la oferta indicada por parte del usuario logueado
     *
     * @param string $ciudad El slug de la ciudad a la que pertenece la oferta
     * @param string $slug El slug de la oferta
     */
    public function comprarAction($ciudad, $slug)
    {
        $em = $this->getDoctrine()->getEntityManager();
        $usuario = $this->get('security.context')->getToken()->getUser();
        
        // Solo pueden comprar los usuarios registrados y logueados
        if (null == $usuario || !$this->get('security.context')->isGranted('ROLE_USUARIO')) {
            $this->get('session')->setFlash('info',
                'Antes de comprar debes registrarte o conectarte con tu usuario y contraseña.'
            );
            return $this->redirect($this->generateUrl('usuario_login'));
        }
        
        // Comprobar que existe la ciudad indicada
        $ciudad = $em->getRepository('CiudadBundle:Ciudad')->findOneBySlug($ciudad);
        if (!$ciudad) {
            throw $this->createNotFoundException('La ciudad indicada no está disponible');
        }
        
        // Comprobar que existe la oferta indicada
        $oferta = $em->getRepository('OfertaBundle:Oferta')->findOneBy(array('ciudad' => $ciudad->getId(), 'slug' => $slug));
        if (!$oferta) {
            throw $this->createNotFoundException('La oferta indicada no está disponible');
        }
        
        // Un mismo usuario no puede comprar dos veces la misma oferta
        $venta = $em->getRepository('OfertaBundle:Venta')->findOneBy(array(
            'oferta'  => $oferta->getId(),
            'usuario' => $usuario->getId()
        ));
        
        if (null != $venta) {
            $fechaVenta = $venta->getFecha();
            
            $formateador = \IntlDateFormatter::create(
                $this->get('translator')->getLocale(),
                \IntlDateFormatter::LONG,
                \IntlDateFormatter::NONE
            );
            
            $this->get('session')->setFlash('error',
                'No puedes volver a comprar la misma oferta (la compraste el '.$formateador->format($fechaVenta).').'
            );
            
            return $this->redirect(
                $this->getRequest()->headers->get('Referer', $this->generateUrl('portada'))
            );
        }
        
        // Guardar la nueva venta e incrementar el contador de compras de la oferta
        $venta = new Venta();
        
        $venta->setOferta($oferta);
        $venta->setUsuario($usuario);
        $venta->setFecha(new \DateTime());
        
        $em->persist($venta);
        
        $oferta->setCompras($oferta->getCompras()+1);
        
        $em->flush();
        
        return $this->render('UsuarioBundle:Default:comprar.html.twig', array(
            'oferta'  => $oferta,
            'usuario' => $usuario
        ));
    }

    public function registroAction()
    {
        $peticion = $this->getRequest();

        $usuario = new Usuario();
        $usuario->setPermiteEmail(true);
        $usuario->setFechaNacimiento(new \DateTime('today - 18 years'));

        $formulario = $this->createForm(new UsuarioType(), $usuario);

        if ($peticion->getMethod() == 'POST') {
            $formulario->bindRequest($peticion);

            if ($formulario->isValid()) {
                $encoder = $this->get('security.encoder_factory')
                                ->getEncoder($usuario);
                $usuario->setSalt(md5(time()));
                $passwordCodificado = $encoder->encodePassword(
                    $usuario->getPassword(),
                    $usuario->getSalt()
                );
                $usuario->setPassword($passwordCodificado);

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($usuario);
                $em->flush();

                $this->get('session')->setFlash('info',
                    '¡Enhorabuena! Te has registrado correctamente en Cupon'
                );

                $token = new UsernamePasswordToken(
                    $usuario,
                    $usuario->getPassword(),
                    'usuarios',
                    $usuario->getRoles()
                );
                $this->container->get('security.context')->setToken($token);

                return $this->redirect($this->generateUrl('portada', array(
                    'ciudad' => $usuario->getCiudad()->getSlug()
                )));
            }
        }

        return $this->render(
            'UsuarioBundle:Default:registro.html.twig',
            array('formulario' => $formulario->createView())
        );
    }

    public function bajaAction()
    {
        $usuario = $this->get('security.context')->getToken()->getUser();

        if (null == $usuario || !$this->get('security.context')->isGranted('ROLE_USUARIO')) {
            $this->get('session')->setFlash('info',
                'Para darte de baja primero tienes que conectarte.'
            );
            return $this->redirect($this->generateUrl('usuario_login'));
        }

        $this->get('request')->getSession()->invalidate();
        $this->get('security.context')->setToken(null);

        $em = $this->getDoctrine()->getEntityManager();
        $em->remove($usuario);
        $em->flush();

        return $this->redirect($this->generateUrl('portada'));
    }

    public function perfilAction()
    {
        $usuario = $this->get('security.context')->getToken()->getUser();
        $formulario = $this->createForm(new UsuarioType(), $usuario);

        $peticion = $this->getRequest();

        if ($peticion->getMethod() == 'POST') {
            $passwordOriginal = $formulario->getData()->getPassword();

            $formulario->bindRequest($peticion);

            if ($formulario->isValid()) {
                if (null == $usuario->getPassword()) {
                    $usuario->setPassword($passwordOriginal);
                }
                else {
                    $encoder = $this->get('security.encoder_factory')
                                    ->getEncoder($usuario);
                    $passwordCodificado = $encoder->encodePassword(
                        $usuario->getPassword(),
                        $usuario->getSalt()
                    );
                    $usuario->setPassword($passwordCodificado);
                }

                $em = $this->getDoctrine()->getEntityManager();
                $em->persist($usuario);
                $em->flush();

                $this->get('session')->setFlash('info',
                    'Los datos de tu perfil se han actualizado correctamente'
                );
                return $this->redirect($this->generateUrl('usuario_perfil'));
            }
        }

        return $this->render('UsuarioBundle:Default:perfil.html.twig', array(
                'usuario' => $usuario,
                'formulario' => $formulario->createView()
        ));
    }
}